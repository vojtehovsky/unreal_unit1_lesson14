#include <iostream>
#include <string>

int main()
{
    std::string text = "The text, which will be shown on console.";
    // std::cin >> text;
    // std::getline(std::cin, text);
    char firstChar = text.front();
    char lastChar = text.back();
    int length = text.length();
    std::cout << text << "\n";
    std::cout << "length: " << length << "\n";
    std::cout << "first char: " << firstChar << " last char: " << lastChar;
}
